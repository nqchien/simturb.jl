!****************************************************************
      PROGRAM couette 
!****************************************************************

!****************************************************************
!
! 2D Couette flow solved using half symmetry.
! Based on the Prandtl turbulence model.
! Written in GNU FORTRAN.
! Input/output is in SI units.
! Written by Sal Rodriguez, March 30, 2014.  Version 1.0.
! Revised by Sal Rodriguez, September 18, 2019.  Version 1.1.
!
!****************************************************************

      implicit none 

      integer i, j, k, k1, nodesx, nodesy
      integer iplt, icycle, ncycle, nprint, nocbp
      real kappa, deltax, deltay, length, height
      real uwall
      real dtdx, dtdy, dtdx2, dtdy2
      real deltat, tend, time

      parameter(length=0.1, height=0.05)
      parameter(nodesx=60, nodesy=6, nprint=20)
      parameter(deltat=1.0e-4, tend=3.5)

      double precision rold(0:nodesx+1,0:nodesy+1)
      double precision rnew(0:nodesx+1,0:nodesy+1)
      double precision uold(0:nodesx+1,0:nodesy+1)
      double precision unew(0:nodesx+1,0:nodesy+1)
      double precision x(0:nodesx+1,0:nodesy+1)
      double precision y(0:nodesx+1,0:nodesy+1)
      double precision re(0:nodesx+1,0:nodesy+1)

      real sigmak, cd, pmax, dmax
      double precision kold(0:nodesx+1,0:nodesy+1)
      double precision knew(0:nodesx+1,0:nodesy+1)
      double precision prodo(0:nodesx+1,0:nodesy+1)
      double precision prodn(0:nodesx+1,0:nodesy+1)
      double precision dissn(0:nodesx+1,0:nodesy+1)
      double precision disso(0:nodesx+1,0:nodesy+1)
      double precision lchar(0:nodesy+1)
      double precision mutn(0:nodesx+1,0:nodesy+1)
      double precision muto(0:nodesx+1,0:nodesy+1)

      real dthmin, dttmin
      real rini, uini
      real rmin, umin
      real rmax, umax
      real mumax
      real vismin, vismax, visc, mu
      real remin, remax
      real zero, tstop, tstart

!****************************************************************
! nodesx - number of nodes in x-direction.
! nodesy - number of nodes in y-direction.
! icycle - counts the number of cycles that have elapsed
!   (1-ncycle).
! ncycle - number of cycles that will be needed to run problem to
!   end time.
! iplt - counts number of times plotting has occured (1-nprint).
! nprint - used to divide the time domain into 1/nprint equal 
!   time intervals, at which a point a plot is generated.
! nocbp - number of cycles before plot is generated = 
!   ncycle/nprint.
! kappa = Kolmogorov's constant. https://glossary.ametsoc.org/wiki/Kolmogorov_constant
!****************************************************************

! Initialize the variables, input parameters.
      zero = 0.0 
      tstart = secnds(zero) 
 
      cd = 0.07 ! Based on Emmons, 1954 and Glushko, 1965.
      visc = 2.02e-5
      rini = 0.16
      mu = visc/rini
      uini = zero
      uwall = -189.0

      kappa = 0.41
      sigmak = 1.0
      pmax = zero
      dmax = zero

      rmin = rini
      umin = zero
      vismin = zero
      remin = zero
      rmax = zero
      umax = zero
      vismax = zero
      mumax = zero
      remax = zero

      deltax = length/(nodesx+1)
      deltay = height/(nodesy+1)
      dtdx = deltat/(2*deltax)
      dtdy = deltat/(2*deltay)
      dtdx2 = deltat/(deltax**2)
      dtdy2 = deltat/(deltay**2)
      print *, 'deltax, deltay, deltat = ', deltax, deltay, deltat
      write(*,*) ' '
      time = zero 
      icycle = 1 
      iplt = 0 
! By starting with k1 = 100, the Matlab output files are written 
! with a number greater than 6.  If k1 = 1, then this will result 
! in file fort.6 being written as soon as k1 reaches 6 and only 
! the first six time updates are shown on the screen.  Curious 
! FORTRAN issue as a result of pre-allocations for "write" and 
! "print" of output.
      k1 = 100
      ncycle = int(tend/deltat)
      nocbp = int(ncycle/nprint) ! Number of cycles before plot

! Initial conditions, spacing (do for all nodes)
      do 50 i=0,nodesx+1
      do 55 j=0,nodesy+1
          rold(i,j) = rini 
          uold(i,j) = uini
          rnew(i,j) = rini
          unew(i,j) = uini
          re(i,j) = zero
          x(i,j) = x(i,j) + i*deltax
          y(i,j) = y(i,j) + j*deltay

          kold(i,j) = 1.0e-4
          knew(i,j) = 1.0e-4
          prodo(i,j) = 0.0
          prodn(i,j) = 0.0
          dissn(i,j) = 1.0e-16
          disso(i,j) = 1.0e-16
          muto(i,j) = 1.0e-3
          mutn(i,j) = 1.0e-3
          lchar(j) = 1.0e-6
 55   continue
 50   continue 

! BEGIN TIME ADVANCEMENT. 
      do 1000 k=1,ncycle 

! Momentum conservation calculation.
               do 300 i = 1,nodesx
               do 305 j = 1,nodesy
                      unew(i,j) = uold(i,j) 
     &                  + (dtdy2*(visc + muto(i,j))/rold(i,j))
     &                  *(uold(i,j+1) - 2*uold(i,j) + uold(i,j-1))
     &                  +( dtdy*(uold(i,j+1)-uold(i,j-1)) )
     &                  *( dtdy*(muto(i,j+1)-muto(i,j-1)) )
! The next three lines are commented because the terms are small 
! for this Couette problem.
!      &                  - dtdx*uold(i,j)*(uold(i+1,j) - uold(i-1,j))
!      &                  + (dtdx2*visc/rold(i,j))
!      &                  *(uold(i+1,j) - 2*uold(i,j) + uold(i-1,j))
 305          continue
 300          continue

! Turbulent kinetic energy calculation, including turbulence viscosity.
            do 400 i = 1,nodesx
            do 405 j = 1,nodesy
                   lchar(j) = kappa*deltay*j
                   knew(i,j) = kold(i,j) + prodo(i,j) - disso(i,j)
     &                  + (dtdy2*(visc+(muto(i,j)/sigmak))/rold(i,j))
     &                  *(kold(i,j+1) - 2*kold(i,j) + kold(i,j-1))
     &                  + (1/sigmak)*dtdy*( muto(i,j+1)-muto(i,j-1) )
     &                  *dtdy*( kold(i,j+1)-kold(i,j-1) )
! The next three lines are commented because the terms are small 
! for this Couette problem.
!     &                  - dtdx*uold(i,j)*(kold(i+1,j) - kold(i-1,j)) 
!     &                  + (dtdx2*(visc+(muto(i,j)/sigmak))/rold(i,j)) 
!     &                  *(kold(i+1,j) - 2*kold(i,j) + kold(i-1,j))

                   if (knew(i,j) .lt. zero) then
                     print *, 'Calc. aborted. Neg. knew = ', knew(i,j)
                     print *, 'Cycle = ', ncycle
                     stop
                   end if

                   mutn(i,j) = rnew(i,j)*lchar(j)*sqrt(knew(i,j))
                   dissn(i,j) = cd*( (knew(i,j)**(3./2.)) )/lchar(j)
                   prodn(i,j) = (mutn(i,j)/rnew(i,j))
     &                      *( dtdy*(unew(i,j+1) - unew(i,j-1)) )**2

                   if (prodn(i,j) .lt. zero) then
                     print *, 'Calc. aborted. Neg. prod. = ', prodn(i,j)
                     print *, 'Cycle = ', ncycle
                     stop
                   end if

                   if(prodn(i,j) .gt. pmax) pmax = prodn(i,j)
                   if(dissn(i,j) .gt. dmax) dmax = dissn(i,j)
                   if(mutn(i,j) .gt. mumax) mumax = mutn(i,j)

 405          continue
 400          continue

! Boundary conditions.
! LHS. Open BC.
         do 500 j=0,nodesy+1
                rnew(0,j) = rnew(1,j) 
                unew(0,j) = unew(1,j)
                knew(0,j) = knew(1,j)
                dissn(0,j) = dissn(1,j)

! RHS.  Open BC. 
                rnew(nodesx+1,j) = rnew(nodesx,j)
                unew(nodesx+1,j) = unew(nodesx,j)
                knew(nodesx+1,j) = knew(nodesx,j)
                dissn(nodesx+1,j) = dissn(nodesx,j)
 500     continue

         do 510 i=0,nodesx+1
! Top. 
                rnew(i,nodesy+1) = rini 
! 'Symmetry' at symmetry plane acts like wall.
                unew(i,nodesy+1) = zero 
                knew(i,nodesy+1) = zero
                dissn(i,nodesy+1) = zero

! Bottom. 
                rnew(i,0) = rini
                unew(i,0) = uwall
                knew(i,0) = 1.0e-4
                dissn(i,0) = 1.0e-8
 510     continue

! Advance the primitive variables in time by swapping the new 
! values into the old arrays.  This is a neat trick to save on 
! memory space.
                do 1300 i=0,nodesx+1 
                do 1305 j=0,nodesy+1
                   rold(i,j) = rnew(i,j) 
                   uold(i,j) = unew(i,j)
                   kold(i,j) = knew(i,j)
                   muto(i,j) = mutn(i,j)
                   disso(i,j) = dissn(i,j)
                   prodo(i,j) = prodn(i,j)

! Get min and max density; max velocity. 
                   if(rnew(i,j) .gt. rmax) rmax = rnew(i,j) 
                   if(abs(unew(i,j)) .gt. abs(umax)) umax = unew(i,j)

                   if(rnew(i,j) .lt. rmin .and. i.ne.nodesx+1) 
     &               rmin = rnew(i,j) 

                   if (kold(i,j) .lt. zero) then
                     print *, 'Calc aborted. Neg. kold = ', kold(i,j)
                     stop
                   end if
1305       continue
1300       continue 

! Calculate the Reynolds number.
                do 1400 i=0,nodesx+1
                do 1405 j=0,nodesy+1
                    re(i,j) = unew(i,j)*deltax*visc/rnew(i,j)
                    if(re(i,j) .gt. remax) remax = re(i,j) 
                    if(re(i,j) .lt. remin) remin = re(i,j)
 1405  continue 
 1400  continue

c Advance time and write plot data if requested.
         time = time + deltat 

         if (mod(icycle,nocbp).eq.0 .and. icycle.gt.0) then 
      !      call matplt(nodesx,nodesy,unew,knew,prodn,dissn,mutn,time,k1)
           iplt = iplt + 1 
         end if 

         icycle = icycle + 1 

1000  continue 

! Write geometric data for matlab input.
5001  format(2(f18.4,1x), f18.3, 1x, f8.5) 
      ! open(unit=5000, file='geodatamax.m') 
      ! write(5000,5001) rmax, umax, remax, length
      ! close(5000)
      ! open(unit=5010, file='geodatamin.m') 
      ! write(5010,5001) rmin, umin, remin, length
      ! close(5010)

      dthmin = rnew(nodesx/2,nodesy/2)*deltax**2/visc
      dttmin = (deltax**2)/mumax

      tstop = secnds(tstart) 

      print *, ' '
      print *, 'Total cpu time = ', tstop/3600.d0, ' hours...' 
      print *, 'or', tstop/60.d0, ' minutes...'
      print *, 'or', tstop, ' seconds.' 
      print *, ' ' 
      print *, 'Minimum density = ', rmin 
      print *, 'Maximum density = ', rmax
      print *, 'Maximum velocity = ', umax 
      print *, 'Min hydro dt = ', dthmin
      print *, 'Min turbulence dt = ', dttmin
      print *, 'Max production = ', pmax
      print *, 'Max dissipation = ', dmax
      print *, 'Max turbulence kinematic viscosity = ', mumax
      print *, ' ' 
      print *, 'Simulation reached completion time of ', tend, 
     &   'seconds'
      print *, ' '

      stop 
      end 
!****************************************************************


!**************************************************************** 
! Generate MATLAB plot files.
!
! Note the following FORTRAN vs. MATLAB matrix numbering:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! FORTRAN vs. MATLAB matrix numbering: !!
!! In FORTRAN:                          !!
!! T = [ (3,1)  (3,2)  (3,3)            !!
!!       (2,1)  (2,2)  (2,3)            !!
!!       (1,1)  (1,2)  (1,3) ]          !!
!!                                      !!
!! In MATLAB:                           !!
!! T = [ (1,1)  (1,2)  (1,3)            !!
!!       (2,1)  (2,2)  (2,3)            !!
!!       (3,1)  (3,2)  (3,3) ]          !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!****************************************************************
 
       subroutine matplt(nodesx,nodesy,unew,knew,prod,diss,mutn,time,k1) 

! Writes Matlab-type surf files for knew, prod, diss, unew, and mutn.
       
       implicit none 
       
       integer i, j, nodesx, nodesy, k1
       real time
       DOUBLE PRECISION knew(0:nodesx+1,0:nodesy+1)
       DOUBLE PRECISION prod(0:nodesx+1,0:nodesy+1)
       DOUBLE PRECISION diss(0:nodesx+1,0:nodesy+1)
       DOUBLE PRECISION unew(0:nodesx+1,0:nodesy+1)
       DOUBLE PRECISION mutn(0:nodesx+1,0:nodesy+1)
       
! var(#) = number of variables with size 4.
! seg size determines the total number of Matlab files that can 
! be written.  *3 allows for up to 999.
       CHARACTER seg*3, var(5)*4
       
       data var/'knew', 'prod', 'diss', 'unew', 'mutn'/

!****************************************************************

       write(*,*) 'Plot-edit time = ', time
       k1 = k1 + 1 
       write (seg, '(i3.3)') k1

! Generate Matlab surf file for the velocity.
       OPEN(UNIT=k1, FILE='ftn/unew' //seg// '.m') 
       DO 8330 I=0,nodesx+1
         WRITE(k1, *) ' Z(:,',i+1,')=['
         DO 8331 J=0,nodesy+1
            WRITE (k1, *) unew(I,J)
 8331  CONTINUE
       WRITE (k1, *) ' ];'
 8330  CONTINUE
        CLOSE(k1)

! Generate Matlab surf file for the turbulence kinetic energy.
       OPEN(UNIT=k1, FILE='ftn/knew' //seg// '.m')
       DO 8430 I=0,nodesx+1
         WRITE(k1, *) ' Z(:,',i+1,')=['
         DO 8431 J=0,nodesy+1
            WRITE (k1, *) knew(I,J)
 8431  CONTINUE
       WRITE (k1, *) ' ];'
 8430  CONTINUE
       CLOSE(k1)

! Generate Matlab surf file for the turbulence production term.
       OPEN(UNIT=k1, FILE='ftn/prod' //seg// '.m')
       DO 8440 I=0,nodesx+1
         WRITE(k1, *) ' Z(:,',i+1,')=['
         DO 8441 J=0,nodesy+1
            WRITE (k1, *) prod(I,J)
 8441  CONTINUE
       WRITE (k1, *) ' ];'
 8440  CONTINUE
       CLOSE(k1)

! Generate Matlab surf file for the turbulence dissipation term.
       OPEN(UNIT=k1, FILE='ftn/diss' //seg// '.m')
       DO 8435 I=0,nodesx+1
         WRITE(k1, *) ' Z(:,',i+1,')=['
         DO 8436 J=0,nodesy+1
            WRITE (k1, *) diss(I,J)
 8436  CONTINUE
       WRITE (k1, *) ' ];'
 8435  CONTINUE
       CLOSE(k1)
 
! Generate Matlab surf file for the turbulence kinematic viscosity.
       OPEN(UNIT=k1, FILE='ftn/mutn' //seg// '.m')
       DO 8530 I=0,nodesx+1
         WRITE(k1, *) ' Z(:,',i+1,')=['
         DO 8531 J=0,nodesy+1
            WRITE (k1, *) mutn(I,J)
 8531  CONTINUE
       WRITE (k1, *) ' ];'
 8530  CONTINUE
       CLOSE(k1)
 
       return
       END

!****************************************************************
