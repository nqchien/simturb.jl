# 2D Couette flow solved using half symmetry.
# Based on the Prandtl turbulence model.
# Originally in GNU FORTRAN.
# Written by Sal Rodriguez, March 30, 2014.  Version 1.0.
# Revised by Sal Rodriguez, September 18, 2019.  Version 1.1.
# Input/output is in SI units.


using Printf


length = 0.1
height = 0.05
nx = 60     # number of nodes in x-direction.
ny = 6      # number of nodes in y-direction.
nprint = 20     # divide the time domain into 1/nprint equal 
                #   time intervals, at which a point a plot is generated.
Δt = 1.0e-4
tend = 3.5


# Initialize the variables, input parameters.

Cd = 0.07  # (Emmons, 1954) and (Glushko, 1965)
visc = 2.02e-5
r0 = 0.16
μ = visc / r0
u0 = 0.0
uwall = -189.0

κ = 0.41    # Kolmogorov's constant, https://glossary.ametsoc.org/wiki/Kolmogorov_constant
σk = 1.0

rₒ = zeros(nx + 2, ny + 2) .+ r0
r = zeros(nx + 2, ny + 2) .+ r0
uₒ = zeros(nx + 2, ny + 2) .+ u0
u = zeros(nx + 2, ny + 2) .+ u0
x = zeros(nx + 2, ny + 2)
y = zeros(nx + 2, ny + 2)
Re = zeros(nx + 2, ny + 2)
kₒ = zeros(nx + 2, ny + 2) .+ 1.0e-4
k = zeros(nx + 2, ny + 2) .+ 1.0e-4
ℙₒ = zeros(nx + 2, ny + 2)
ℙ = zeros(nx + 2, ny + 2)
ⅅₒ = zeros(nx + 2, ny + 2) .+ 1.0e-16
ⅅ = zeros(nx + 2, ny + 2) .+ 1.0e-16
μtₒ = zeros(nx + 2, ny + 2) .+ 1.0e-3
μt = zeros(nx + 2, ny + 2) .+ 1.0e-3
# ℓ = zeros(ny + 2) .+ 1.0e-6  # currently unable to implement 1D version.
ℓ = zeros(nx + 2, ny + 2) .+ 1.0e-6

rmin = r0
umin = 0.0
vismin = 0.0
Remin = 0.0
rmax = 0.0
umax = 0.0
vismax = 0.0
μmax = 0.0
Remax = 0.0
pmax = 0.0
dmax = 0.0

Δx = length/(nx + 1)
Δy = height/(ny + 1)
dtdx = Δt/(2*Δx)
dtdy = Δt/(2*Δy)
dtdx2 = Δt/(Δx^2)
dtdy2 = Δt/(Δy^2)
println("Δx, Δy, Δt = $(Δx), $(Δy), $(Δt)")

time = 0.0
icycle = 1  # number of cycles that have elapsed (1-ncycle)
iplt = 0    # number of times plotting has occured (1-nprint).
# By starting with k1 = 100, the Matlab output files are written 
# with a number greater than 6.  If k1 = 1, then this will result 
# in file fort.6 being written as soon as k1 reaches 6 and only 
# the first six time updates are shown on the screen.  Curious 
# FORTRAN issue as a result of pre-allocations for "write" and 
# "print" of output.
k1 = 100
ncycle = Int(round(tend/Δt))    # number of cycles that will be needed to run problem to end time
nocbp = Int(round(ncycle/nprint))  # number of cycles before plot is generated = ncycle/nprint



""" Writes Matlab-type surf files for k, prod, diss, unew, and μt. """
function matplt(nodesx::Int64, nodesy::Int64, 
                unew::Matrix{Float64}, knew::Matrix{Float64}, 
                prod::Matrix{Float64}, diss::Matrix{Float64}, 
                μt::Matrix{Float64}, time::Float64, k1::Int64) 
    # Note the following FORTRAN vs. MATLAB matrix numbering:
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # !! FORTRAN vs. MATLAB matrix numbering: !!
    # !! In FORTRAN:                          !!
    # !! T = [ (3,1)  (3,2)  (3,3)            !!
    # !!       (2,1)  (2,2)  (2,3)            !!
    # !!       (1,1)  (1,2)  (1,3) ]          !!
    # !!                                      !!
    # !! In MATLAB:                           !!
    # !! T = [ (1,1)  (1,2)  (1,3)            !!
    # !!       (2,1)  (2,2)  (2,3)            !!
    # !!       (3,1)  (3,2)  (3,3) ]          !!
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    # integer i, j, nodesx, nodesy, k1
    # real time
    # DOUBLE PRECISION knew(0:nodesx+1,0:nodesy+1)
    # DOUBLE PRECISION prod(0:nodesx+1,0:nodesy+1)
    # DOUBLE PRECISION diss(0:nodesx+1,0:nodesy+1)
    # DOUBLE PRECISION unew(0:nodesx+1,0:nodesy+1)
    # DOUBLE PRECISION μt(0:nodesx+1,0:nodesy+1)


    println("Plot-edit time = $(time)")
    k1 = k1 + 1 
    #   write (seg, '(i3.3)') k1

    path = "./res/"
    # path = "./1eq/Prandtl/res/"

    # Generate Matlab surf file for the velocity
    open(path * "unew$(k1).m", "w") do file
        for i = 1 : nodesx + 2
            println(file, "Z(:,$(i+1)) = [")
            for j = 1 : nodesy + 2
                println(file, unew[i, j])
            end
            println(file, "];")
        end
    end
    
    # Generate Matlab surf file for the turbulence kinetic energy
    open(path * "knew$(k1).m","w") do file
        for i = 1 : nodesx + 2
            println(file, "Z(:,$(i+1)) = [")
            for j = 1 : nodesy + 2
                println(file, knew[i, j])
            end
            println(file, "];")
        end
    end

    # Generate Matlab surf file for the turbulence production term
    open(path * "prod$(k1).m","w") do file
        for i = 1 : nodesx + 2
            println(file, "Z(:,$(i+1)) = [")
            for j = 1 : nodesy + 2
                println(file, prod[i, j])
            end
            println(file, "];")
        end
    end

    # Generate Matlab surf file for the turbulence dissipation term
    open(path * "diss$(k1).m","w") do file
        for i = 1 : nodesx + 2
            println(file, "Z(:,$(i+1)) = [")
            for j = 1 : nodesy + 2
                println(file, diss[i, j])
            end
            println(file, "];")
        end
    end

    # Generate Matlab surf file for the turbulence kinematic viscosity.
    open(path * "μt$(k1).m","w") do file
        for i = 1 : nodesx + 2
            println(file, "Z(:,$(i+1)) = [")
            for j = 1 : nodesy + 2
                println(file, μt[i, j])
            end
            println(file, "];")
        end
    end
end


function calc_print()
    # Initial conditions, spacing (for all nodes)
    global u, uₒ, rₒ, r, k, kₒ, ℙ, ℙₒ, ⅅ, ⅅₒ, μt, μtₒ, ℓ, Re
    
    # TIME MARCH
    for _ = 1 : ncycle 

        # Momentum conservation calculation
        u[2:nx+1, 2:ny+1] .= uₒ[2:nx+1, 2:ny+1] .+
                ( dtdy2*(visc .+ μtₒ[2:nx+1, 2:ny+1]) ./ rₒ[2:nx+1, 2:ny+1]) .*
                ( uₒ[2:nx+1, 3:ny+2] .- 2*uₒ[2:nx+1, 2:ny+1] .+ uₒ[2:nx+1, 1:ny]) .+
                ( dtdy * (uₒ[2:nx+1, 3:ny+2] .- uₒ[2:nx+1, 1:ny]) ) .*
                ( dtdy * (μtₒ[2:nx+1, 3:ny+2] .- μtₒ[2:nx+1, 1:ny]) )
                
        # Turbulent kinetic energy calculation, including turbulence viscosity

        for j = 2 : ny+1
            for i = 2 : nx+1
                ℓ[i, j] = κ * Δy * j
            end
        end  # currently unable to populate using collect and repeat
        # ℓ = κ * Δy * collect(2 : ny+1)
        # ℓ = κ * Δy * collect(1 : ny+2)
        # ℓ = reshape(repeat(ℓ, nx+2, 1), [nx+2, ny+2])

        k[2:nx+1, 2:ny+1] .= kₒ[2:nx+1, 2:ny+1] .+
                ℙₒ[2:nx+1, 2:ny+1] .- ⅅₒ[2:nx+1, 2:ny+1] .+
                (dtdy2*(visc .+ (μtₒ[2:nx+1, 2:ny+1]/σk))./rₒ[2:nx+1, 2:ny+1]) .*
                (kₒ[2:nx+1, 3:ny+2] .- 2*kₒ[2:nx+1, 2:ny+1] .+ kₒ[2:nx+1, 1:ny]) .+
                (1/σk) * dtdy * ( μtₒ[2:nx+1, 3:ny+2] .- μtₒ[2:nx+1, 1:ny] ) .*
                dtdy .* ( kₒ[2:nx+1, 3:ny+2] .- kₒ[2:nx+1, 1:ny] )

        if any(k[2:nx+1, 2:ny+1] .< 0.0)
            println("k < 0 Cycle = $(ncycle)")
            error("Calc. aborted.")
        end

        # M^(3/2) not allowed for a non-square matrix
        μt[2:nx+1, 2:ny+1] .= r[2:nx+1, 2:ny+1] .* ℓ[2:nx+1, 2:ny+1] .* sqrt.(k[2:nx+1, 2:ny+1])
        ⅅ[2:nx+1, 2:ny+1] .= Cd * ( (k[2:nx+1, 2:ny+1] .* sqrt.(k[2:nx+1, 2:ny+1])) ) ./ ℓ[2:nx+1, 2:ny+1]

        ℙ[2:nx+1, 2:ny+1] .= (μt[2:nx+1, 2:ny+1] ./ r[2:nx+1, 2:ny+1]) .* ( dtdy * (u[2:nx+1, 3:ny+2] .- u[2:nx+1, 1:ny]) ).^2

        if any(ℙ[2:nx+1, 2:ny+1] .< 0.0)
            println("ℙ < 0 Cycle = $(ncycle)")
            error("Calc. aborted.")
        end


        # for j = 2 : ny + 1
        #     for i = 2 : nx + 1
        #         ℓ[j] = κ * Δy * j
        #         k[i, j] = kₒ[i, j] + ℙₒ[i, j] - ⅅₒ[i, j] + 
        #                 (dtdy2*(visc + (μtₒ[i, j]/σk))/rₒ[i, j]) *
        #                 (kₒ[i, j+1] - 2*kₒ[i, j] + kₒ[i, j-1]) +
        #                 (1/σk) * dtdy * ( μtₒ[i, j+1] - μtₒ[i, j-1] ) *
        #                 dtdy * ( kₒ[i, j+1] - kₒ[i, j-1] )

        #         if (k[i, j] < 0)
        #             print("Neg. k = $(k[i, j]) ")
        #             println("Cycle = $(ncycle)")
        #             error("Calc. aborted.")
        #         end

        #         μt[i, j] = r[i, j] * ℓ[j] * sqrt(k[i, j])
        #         ⅅ[i, j] = Cd * ( (k[i, j]^(3/2)) ) / ℓ[j]
        #         ℙ[i, j] = (μt[i, j] / r[i, j]) *
        #                     ( dtdy * (u[i, j+1] - u[i, j-1]) )^2

        #         if (ℙ[i, j] < 0)
        #             print("Calc. aborted. Neg. P = $(ℙ[i, j]) ")
        #             println("Cycle = $(ncycle)")
        #             return
        #         end
        #     end
        # end

        # Get maxima of production, dissipation, and viscosity
        global pmax, dmax, μmax
        pmax = maximum(ℙ)
        dmax = maximum(ⅅ)
        μmax = maximum(μt)

        # Boundary conditions

        # LHS open BC
        r[1, :] = r[2, :]
        u[1, :] = u[2, :]
        k[1, :] = k[2, :]
        ⅅ[1, :] = ⅅ[2, :]

        # RHS open BC
        r[nx+2, :] = r[nx+1, :]
        u[nx+2, :] = u[nx+1, :]
        k[nx+2, :] = k[nx+1, :]
        ⅅ[nx+2, :] = ⅅ[nx+1, :]
        
        # Top BC - 'Symmetry' at symmetry plane acts like wall
        r[:, ny+2] .= r0
        u[:, ny+2] .= 0.0 
        k[:, ny+2] .= 0.0
        ⅅ[:, ny+2] .= 0.0

        # Bottom BC
        r[:, 1] .= r0
        u[:, 1] .= uwall
        k[:, 1] .= 1.0e-4
        ⅅ[:, 1] .= 1.0e-8
        
        # Save old values before advancing to the next time step
        rₒ = copy(r)
        uₒ = copy(u)
        kₒ = copy(k)
        μtₒ = copy(μt)
        ⅅₒ = copy(ⅅ)
        ℙₒ = copy(ℙ)

        # Get min and max density; max velocity
        global rmax, umax, rmin, kₒmin
        rmax = maximum(r)
        umax = maximum(abs.(u))
        rmin = minimum(r[1:nx+1, :])
        kₒmin = minimum(kₒ[1:nx+1, :])
        @assert(kₒmin >= 0), "Neg. kₒ = $(kₒmin). Calculation aborted."

        # Calculate the Reynolds number
        global Remax, Remin
        Re = u .* Δx * visc / r
        Remax = maximum(Re)
        Remin = minimum(Re)

        # Advance time and write plot data if requested
        global time = time + Δt 

        global icycle, nocbp, iplt
        if (icycle % nocbp == 0 && icycle > 0)
            # matplt(nodesx, nodesy, u, k, ℙ, ⅅ, μt, time, k1)
            iplt = iplt + 1
        end
        icycle = icycle + 1

    end

    # Write geometric data for matlab input
    # open("geodatamax.m","w") do file
    #     @printf(file, "%18.4f %18.4f %18.3f %8.5f\n", rmax, umax, Remax, length)
    # end

    # open("geodatamin.m","w") do file
    #     @printf(file, "%18.4f %18.4f %18.3f %8.5f\n", rmin, umin, Remin, length)
    # end

    dthmin = r[nx ÷ 2 + 1, ny ÷ 2 + 1] * Δx^2 / visc
    dttmin = Δx^2 / μmax


    println("Minimum density = $(rmin)")
    println("Maximum density = $(rmax)")
    println("Maximum velocity = $(umax)")
    println("Min hydro dt = $(dthmin)")
    println("Min turbulence dt = $(dttmin)")
    println("Max production = $(pmax)")
    println("Max dissipation = $(dmax)")
    println("Max turbulence kinematic viscosity = $(μmax)")
    println()
    println("Simulation reached completion time of $(tend) seconds.")

end

calc_print()
