
Sal Rodriguez's Fortran code to estimate turbulence characteristics by Prandtl's one-equation model

From https://www.cfdturbulence.com/

This FORTRAN program lays the basic fundamentals for programming turbulence models to solve realistic turbulent flows. As a fundamental, extendable platform, the program uses the Prandtl turbulence model to consider a 1D Couette flow. In this specific situation, the program solves the x-momentum equation for the u-velocity and the turbulent kinetic energy (k) partial differential equation. Key output are turbulence velocity, dissipation, and kinetic energy. Extending the program to 2D and 3D is rather straightforward by adding the y and z momentum equations, and noting that the generalized momentum PDEs should be used, as found in chapter 2 of the book. In addition, the Prandtl turbulence model can be replaced with the ω partial differential equation and the relevant 2006 k-ω auxiliary equations, thereby extending the model to the 2006 k-ω Wilcox turbulence model; refer to Section 4.6.3.4 of the book.

Kolmogorov constant is explained at https://glossary.ametsoc.org/wiki/Kolmogorov_constant

