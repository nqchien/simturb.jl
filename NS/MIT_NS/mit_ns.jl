# MIT18086_NAVIERSTOKES
#    Solves the incompressible Navier-Stokes equations in a
#    rectangular domain with prescribed velocities along the
#    boundary. The solution method is finite differencing on
#    a staggered grid with implicit diffusion and a Chorin
#    projection method for the pressure.
#    Visualization is done by a colormap-isoline plot for
#    pressure and normalized quiver and streamline plot for
#    the velocity field.
#    The standard setup solves a lid driven cavity problem.

# 07/2007 by Benjamin Seibold
#            http://www-math.mit.edu/~seibold/
# Feel free to modify for teaching and learning.


using AMD
using SparseArrays
using LinearAlgebra
using CairoMakie


function avg(A, k=1)
    if size(A,1) == 1
        A = A'
    end
    if k < 2
        B = (A[2:end, :] + A[1:end-1,:]) / 2
    else
        B = avg(A, k-1)
    end
    if size(A,2) == 1
        B = B'
    end
    return B
end


function K1(n, h, a11)
    # a11: Neumann=1, Dirichlet=2, Dirichlet mid=3
    matr = [-1 a11 0; ones(n-2,1)*[-1 2 -1]; 0 a11 -1]
    b₋₁ = matr[1:end-1, 1]
    b₀ = matr[:, 2]
    b₊₁ = matr[2:end, 3]
    sp = spdiagm(n, n, -1 => b₋₁, 0 => b₀, 1 => b₊₁)
    A = sp' ./ (h .^ 2)
end


# auxiliary function
function speye(n)
    sparse(1I, n, n)
end


# auxiliary function
function diff₁(matr)
    diff(matr, dims=1)
end


Re = 100     # Reynolds number
dt = 1e-2    # time step
tf = 4e-0   # final time
lx = 1       # width of box
ly = 1       # height of box
nx = 90      # number of x-gridpoints
ny = 90      # number of y-gridpoints
nsteps = 10  # number of steps with graphic output

nt = ceil(tf/dt)
dt = tf / nt
hx = lx / nx
x = 0 : hx : lx
hy = ly / ny
y = 0 : hy : ly
X = x' .* ones(length(y))
Y = ones(length(x)) .* y

# initial condition
U = zeros(nx-1, ny)
V = zeros(nx, ny-1)

# boundary conditions
uN = (x .* 0 .+ 1)'
vN = avg(x) .* 0
uS = (x .* 0)'
vS = avg(x) .* 0
uW = avg(y) .* 0
vW = zeros(size(y))
uE = avg(y) .* 0
vE = zeros(size(y))

Ubc = dt/Re * ([2*uS[2:end-1] zeros(nx-1,ny-2) 2*uN[2:end-1]] / hx^2 +
        [uW; zeros(nx-3,ny);uE] / hy^2)
Vbc = dt/Re * ([vS' zeros(nx,ny-3) vN'] / hx^2 +
        [2*vW[2:end-1]'; zeros(nx-2,ny-1); 2*vE[2:end-1]'] / hy^2)

print("initialization")
Lp = kron(speye(ny), K1(nx,hx,1)) + kron(K1(ny,hy,1), speye(nx))
Lp[1,1] = 3/2 * Lp[1,1]
perp = symamd(Lp)
Rp = cholesky(Lp[perp,perp])
Rpt = Rp'

Lu = speye((nx-1)*ny) + dt/Re*(kron(speye(ny),K1(nx-1,hx,2)) +
        kron(K1(ny,hy,3), speye(nx-1)))
peru = symamd(Lu)
Ru = cholesky(Lu[peru,peru])
Rut = Ru'

Lv = speye(nx*(ny-1)) + dt/Re*(kron(speye(ny-1), K1(nx,hx,3)) +
        kron(K1(ny-1,hy,2), speye(nx)))
perv = symamd(Lv)
Rv = cholesky(Lv[perv,perv])
Rvt = Rv'

Lq = kron(speye(ny-1),K1(nx-1,hx,2)) + kron(K1(ny-1,hy,2),speye(nx-1))
perq = symamd(Lq)
Rq = cholesky(Lq[perq,perq])
Rqt = Rq'

println(", time loop\n--20%--40%--60%--80%-100%")
for k = 1:nt
    global U, V
    # treat nonlinear terms
    γ = min( 1.2 * dt * max(maximum(abs.(U))/hx, maximum(abs.(V))/hy), 1)
    Ue = [uW; U; uE];
    Ue = [2*uS' - Ue[:,1]  Ue  2*uN' - Ue[:,end]]

    Ve = [vS'  V  vN'];
    Ve = [(2*vW - Ve[1,:])'; Ve; (2*vE - Ve[end,:])']
    Ua = avg(Ue')'
    Ud = diff₁(Ue')'/2
    Va = avg(Ve)
    Vd = diff₁(Ve)/2
    UVx = diff₁( Ua.*Va - γ * abs.(Ua) .* Vd ) / hx
    UVy = diff₁((Ua.*Va - γ*Ud .* abs.(Va))')' / hy

    Ua = avg( Ue[:,2:end-1] );
    Ud = diff₁( Ue[:,2:end-1] ) / 2;
    Va = avg( Ve[2:end-1,:]' )';
    Vd = diff₁( Ve[2:end-1,:]')' / 2;
    U2x = diff₁(Ua.^2 - γ*abs.(Ua).*Ud) / hx;
    V2y = diff₁((Va.^2 - γ*abs.(Va).*Vd)')' / hy;
    U = U - dt*(UVy[2:end-1,:] + U2x);
    V = V - dt*(UVx[:,2:end-1] + V2y);
    
    # implicit viscosity
    rhs = reshape(U + Ubc, :, 1)
    u = zeros(size(peru))
    u[peru] = Ru \ ( Rut \ rhs[peru])
    U = reshape(u, nx-1, ny)
    rhs = reshape(V + Vbc, :, 1)
    v = zeros(size(perv))
    v[perv] = Rv \ (Rvt \ rhs[perv])
    V = reshape(v, nx, ny-1)
    
    # pressure correction
    rhs = reshape(diff₁([uW;U;uE])/hx + diff₁([vS' V vN']')'/hy, :, 1)
    p = zeros(size(perp))
    p[perp] = Rp \ (Rpt \ rhs[perp])
    P = -reshape(p, nx, ny)     # Note: negation
    U = U - diff₁(P) / hx
    V = V - diff₁(P')' / hy
    
    # println(sum(Ue), sum(Ve))
    # visualization
    if floor(25*k/nt) > floor(25*(k-1)/nt)
        print('.')
    end
    if k == 1 || (floor(nsteps*k/nt)>floor(nsteps*(k-1)/nt) && k > nt/2)
        f = Figure()
        ax = Axis(f[1, 1]; aspect=DataAspect(), title="Re = $(Re), t = $(k*dt)")
        # stream function
        rhs = reshape(diff₁(U')'/hy - diff₁(V)/hx, :, 1)
        q = zeros(size(perq))
        q[perq] = Rq \ (Rqt \ rhs[perq])
        Q = zeros(nx+1, ny+1)
        Q[2:end-1, 2:end-1] = reshape(q, nx-1, ny-1)
        # clf, contourf(avg(x),avg(y),P',20,'w-'); # hold on
        contour!(x,y,Q'; levels=20, color="black")
        Ue = [uS'  avg([uW; U; uE]' )'  uN']
        Ve = [vW'; avg([vS' V vN']); vE']
        # Len = sqrt(Ue.^2 + Ve.^2 .+ eps())
        # print(size((Ue./Len)'), size((Ve./Len)'))
        # quiver!(x[2:5:end], y[2:5:end], Ue[2:5:end]'./1, Ve[2:5:end]'./1)
        quiver!(x[2:2:end], y[2:2:end], Ue[2:2:end]'./1, Ve[2:2:end]'./1)
        # quiver!(x, y, (Ue./Len)', (Ve./Len)')
        # hold off, axis equal, axis([0 lx 0 ly])
        p = sort(p);  # caxis(p([8 end-7]))
        # f  # show the image, not available for CairoMakie
        save("plot$(Int32(k))-Re$(Re).png", f)
    end
end
println()
