using SparseArrays
using LinearAlgebra

# Generate Laplacian matrix, with 5 point stencil, and
# modifications for the central term, probably adding a diagonal matrix?
# a1 a2 a3 a4 are for denoting the boundary conditions.
function LaplacianFinal(a1,a2,a3,a4,nx,ny,dx,dy)
    return kron(sparse(1I, ny, ny), Laplacian(nx,dx,a1,a2)) + 
            kron(Laplacian(ny,dy,a3,a4), sparse(1I, nx, nx));
end