using SparseArrays;

function Laplacian(n,h,a1,a2)
    # a1/a2: Neumann=1, Dirichlet=2, Dirichlet mid=3;
    matr = [-1 a1 0; ones(n-2,1)*[-1 2 -1]; 0 a2 -1];
    b₋₁ = matr[1:end-1, 1];
    b₀ = matr[:, 2];
    b₊₁ = matr[2:end, 3];
    sp = spdiagm(n, n, -1 => b₋₁, 0 => b₀, 1 => b₊₁);
    A = sp' ./ (h .^ 2);
end