using Printf


@enum FlowMode begin
    Internal = 0  # internal pipe flow
    External = 1  # external flow over flat plate
    end

    
struct LIKE
    ℓ :: Float64  # integral eddy length scale
    I :: Float64  # turbulence intensity
    k :: Float64  # turbulent kinetic energy
    ϵ :: Float64  # turbulent dissipation rate
end

struct Dynamics
    u_ch_max :: Float64  # Max. mean turbulence velocity
    Lₑ :: Float64   # Entrance length for turbulent
    Lₑ′ :: Float64  # ratio Le over x_ch
    Cf :: Float64   # Wall friction
    u🌟 :: Float64  # Wall friction velocity
    τ_wall :: Float64   # Wall shear stress
    y₁ :: Float64   # y at y+=1
    y₇ :: Float64   # y at y+=7
    y₃₀ :: Float64  # y at y+=30
    nu_t :: Float64   # Eddy viscosity
    nu_rat :: Float64   # Ratio of turbulent and fluid viscosity
end

struct Num
    Re :: Float64  # Reynolds number
    Ma :: Float64  # Mach number
end

struct Frequency
    ω :: Float64      # eddy frequency
    f_Kol :: Float64  # Kolmogorov eddy frequency
    f_Tay :: Float64  # Taylor eddy frequency
    f_Int :: Float64  # internal eddy frequency
end

struct Scale
    ℓ_Kol :: Float64  # Kolmogorov eddy size
    v_Kol :: Float64  # Kolmogorov eddy velocity
    t_Kol :: Float64  # Kolmogorov eddy time scale
    ℓ_Tay :: Float64  # Taylor eddy size
    v_Tay :: Float64  # Taylor eddy velocity
    t_Tay :: Float64  # Taylor eddy time scale
    ℓ_Int :: Float64  # Internal eddy size
    v_Int :: Float64  # Internal eddy velocity
    t_Int :: Float64  # Internal eddy time scale
end

"""
Calculate the "LIKE" algorithm and y+ (Rodriguez 2017) with velocity, length, and time scales
Turbulence intensity, k, epsilon, omega, and nu_t are also calculated.      
                                                                            
Units are in SI, unless otherwise noted.                                    
                                                                            
Assumes fully-developed internal pipe flow, or external flow over a flat plate.
                                                                                                  
Concept: Sal B. Rodriguez 02/15/2016
Julia port: Nguyen Q Chien

- Some form of velocity or mass flow rate, e.g., U_inf, u_max, m_dot. 
- Pressure P is required for physical properties that are pressure dependent (if using Refprop).
- Fluid temperature, T_fluid if using Refprop.
- If not using Refprop, then the physical properties must be input manually.
- Some form of characteristic distance, e.g., pipe diameter D, L, or x_char. 
- sCO2 nozzle, based on ASME paper GT2019-91791, by Finn and Dogan, 2019.
- T_crit=304.1 K, P_crit=7.38 MPa, and rho_crit=469 kg/m3.
 
Input: 
    u_ch = fluid characteristic velocity
    x_ch = characteristic length (pipe diameter | distance from plate edge)
    flow = {Internal | External}, default Internal
    λ = surface roughness, default = 0
    ρ_liq = density of fluid, default = 1000 kg/m3
    μ_liq = dynamic viscosity of fluid, default = 8.9e-4 Pa*s
    U_sound = speed of sound of fluid, default = 1480 m/s

Returns:
    LIKE: struct with turbulent properties
    scales: struct with turbulent scales
    freq: struct with turbulent frequencies
"""
function LIKE_yplus(x_ch, u_ch; 
                    flowmode::FlowMode=Internal, λ=0.0, 
                    ρ_liq=1000, μ_liq=8.9E-4, U_sound=1480)
    nu = μ_liq / ρ_liq;
    Ma = u_ch / U_sound;  # Mach number

    ### Constants
    y₊₁ = 1.0;   # Used to obtain y at y+ = 1 for meshing purposes.
    y₊₇ = 7.0;   # Laminar viscous sublayer; 7 is ~ max., but can be as little as 5-ish.
    y₊₃₀ = 30.0; # Value for beginning of log domain.
    Cμ = 0.09;
    β🌟 = 9/100.;  # Wilcox k-ω model

    # Begin turbulence calculations

    u_bar = u_ch;               # u_fluid;
    u_bar_max = (5/4) * u_bar;  # valid iff u_bar is an average velocity
    Re = x_ch * u_ch / nu;

    ℓ = 0.07 * x_ch;
    I = 0.144 * Re^(-0.146);    # Sanborn (1955)
    k = (3/2.) * (u_ch * I)^2;
    ϵ = Cμ * (k^(3 / 2))/ℓ;  # "ℓ" based on ℓ = C * x_ch (Prandtl-Kolmogorov)

    ω = ϵ / (β🌟 * k);
    nu_t = (Cμ * k^2)/ϵ;
    nu_rat = nu_t / nu;

    # Kolmogorov Eddies
    ℓ_Kol = ( nu^3 / ϵ )^(1/4);
    t_Kol = (nu/ϵ)^(1/2);
    v_Kol = ℓ_Kol / t_Kol;

    # Taylor Eddies
    ℓ_Tay = ( 10 * k * nu / ϵ )^(1/2);
    t_Tay = (15 * nu / ϵ) ^ (1/2);
    v_Tay = ℓ_Tay / t_Tay;

    # Integral Eddies
    ℓ_Int = ℓ;  # Fast estimate vs. Equation ℓ = Cμ*(k^3/2)/ϵ
    t_Int = Cμ * k/ϵ;
    v_Int = ℓ_Int / t_Int;

    if flowmode == External
        @assert Re ≤ 1.0e9 "Re outside of wall friction correlation range"
        Cf_ext = (2*log10(Re) - 0.65)^(-2.3);  # Schlichting skin friction equation for flat plate.  Good for Re < 1x10^9.
        Cf = Cf_ext;
    elseif flowmode == Internal
        Cf_int = 0.0055 * ( 1 + (2.0e4*(λ/x_ch) + 1.0e6/Re)^(1/3) );
        Cf = Cf_int/4;   # Convert the Darcy value to Fanning friction factor
    else
        error("Unknown flow type")
    end

    τ_wall = Cf * ρ_liq * u_ch * u_ch/2;
    u🌟 = sqrt(τ_wall / ρ_liq);   # friction velocity, aka shear velocity

    y₁ = y₊₁ * nu / u🌟;
    y₇ = y₊₇ * nu / u🌟;
    y₃₀ = y₊₃₀ * nu / u🌟;

    # Entrance length to reach fully developed turbulent flow
    n₁ = 0.25;
    C₁ = 1.359;
    Lₑ = x_ch * C₁ * Re^n₁;
    Lₑ′ = Lₑ / x_ch;

    # Eddy frequencies
    f_Kol = 1 / t_Kol;
    f_Tay = 1 / t_Tay;
    f_Int = 1 / t_Int;

    LIKE_params = LIKE(ℓ, I, k, ϵ);
    dyn = Dynamics(u_bar_max, Lₑ, Lₑ′, Cf, u🌟, τ_wall, y₁, y₇, y₃₀, nu_t, nu_rat);
    num = Num(Re, Ma);
    turb_scales = Scale(ℓ_Kol, v_Kol, t_Kol, ℓ_Tay, v_Tay, t_Tay, ℓ_Int, v_Int, t_Int);
    turb_freq = Frequency(ω, f_Kol, f_Tay, f_Int);
    
    [LIKE_params, dyn, num, turb_scales, turb_freq];
end


# Main program

# for CO2 fluid
U_sound = 472.9;
μ_liq = 38.8e-6;
ρ_liq = 177.2;

flowmode = Internal;
λ = 0.0;       # surface roughness, m

x_ch = 0.025;   # D for pipe flow, x for flat plate flow
u_ch = 4.67;


results = LIKE_yplus(x_ch, u_ch; 
                    flowmode=flowmode, λ=λ, ρ_liq=ρ_liq, μ_liq=μ_liq, U_sound=U_sound);
like = results[1];
dyn = results[2];
num = results[3];
sc = results[4];
frq = results[5];


function test()
    @assert @sprintf("%3.2e", num.Re) == "5.33e+05" "Re is not correct."
    @assert @sprintf("%3.2e", dyn.u_ch_max) == "5.84e+00" "Max. mean velocity is not correct."
    @assert @sprintf("%3.2e", dyn.Lₑ) == "9.18e-01" "Entrance length for turbulent flow not correct."
    @assert @sprintf("%3.2e", dyn.Lₑ′) == "3.67e+01" "Entrance length (dimensionless) not correct."
    @assert @sprintf("%3.2e", dyn.Cf) == "3.07e-03" "Wall friction Cf not correct."
    @assert @sprintf("%3.2e", dyn.u🌟) == "1.83e-01" "Wall friction velocity not correct."
    @assert @sprintf("%3.2e", dyn.τ_wall) == "5.93e+00" "Wall shear stress not correct."
    @assert @sprintf("%3.2e", dyn.y₁) == "1.20e-06" "y₁ not correct."
    @assert @sprintf("%3.2e", dyn.y₇) == "8.38e-06" "y₇ not correct."
    @assert @sprintf("%3.2e", dyn.y₃₀) == "3.59e-05" "y₃₀ not correct."
    @assert @sprintf("%3.2e", dyn.nu_t) == "2.10e-04" "Turbulent kinematic viscosity not correct."
    @assert @sprintf("%3.2e", dyn.nu_rat) == "9.60e+02" "Turbulent kinematic viscosity ratio not correct."
    @assert @sprintf("%3.2e", like.I) == "2.10e-02" "Turbulent intensity not correct."
    @assert @sprintf("%3.2e", like.k) == "1.44e-02" "Specific turbulence kinetic energy not correct."
    @assert @sprintf("%3.2e", like.ϵ) == "8.91e-02" "Eddy dissipation not correct."
    @assert @sprintf("%3.2e", sc.ℓ_Kol) == "1.85e-05" "Kolmogorov eddy size not correct."
    @assert @sprintf("%3.2e", sc.v_Kol) == "1.18e-02" "Kolmogorov eddy velocity not correct."
    @assert @sprintf("%3.2e", sc.t_Kol) == "1.57e-03" "Kolmogorov eddy time not correct."
    @assert @sprintf("%3.2e", sc.ℓ_Tay) == "5.95e-04" "Taylor eddy size not correct."
    @assert @sprintf("%3.2e", sc.v_Tay) == "9.81e-02" "Taylor eddy velocity not correct."
    @assert @sprintf("%3.2e", sc.t_Tay) == "6.07e-03" "Taylor eddy time not correct."
    @assert @sprintf("%3.2e", sc.ℓ_Int) == "1.75e-03" "Integral eddy size not correct."
    @assert @sprintf("%3.2e", sc.v_Int) == "1.20e-01" "Integral eddy velocity not correct."
    @assert @sprintf("%3.2e", sc.t_Int) == "1.46e-02" "Integral eddy time not correct."
    @assert @sprintf("%3.2e", frq.ω) == "6.86e+01" "Eddy frequency not correct."
    @assert @sprintf("%3.2e", frq.f_Kol) == "6.38e+02" "Kolmogorov frequency not correct."
    @assert @sprintf("%3.2e", frq.f_Tay) == "1.65e+02" "Taylor frequency not correct."
    @assert @sprintf("%3.2e", frq.f_Int) == "6.86e+01" "Integral frequency not correct."
end

function print_res()
    # Characteristic length and velocity.
    @printf("Characteristic length = %3.2e m \n", x_ch)
    @printf("Characteristic velocity = %3.2e m/s \n", u_ch)

    # Fluid properties.
    @printf("Density = %3.2e kg/m³ \n", ρ_liq)
    @printf("Dynamic viscosity = %3.2e Pa.s \n", μ_liq)
    @printf("Kinematic viscosity = %3.2e m²/s \n", μ_liq/ρ_liq)

    # Dimensionless numbers.
    @printf("Reynolds number = %3.2e \n", num.Re)
    @printf("Mach number = %3.2e \n", num.Ma)

    # Key Fluid dynamics and turbulence parameters.
    @printf("Max. mean turbulence velocity = %3.2e m/s \n", dyn.u_ch_max)
    @printf("Entrance length for turbulent flow = %3.2e m \n", dyn.Lₑ)
    @printf("L_e/x_char = %3.2e \n", dyn.Lₑ′)
    @printf("Wall friction Cf or f = %3.2e \n", dyn.Cf)
    @printf("Wall friction velocity = %3.2e m/s \n", dyn.u🌟)
    @printf("Wall shear = %3.2e kg m⁻¹ s⁻² \n", dyn.τ_wall)
    @printf("y at y+=1 = %3.2e m \n", dyn.y₁)
    @printf("y at y+=7 = %3.2e m \n", dyn.y₇)
    @printf("y at y+=30 = %3.2e m \n", dyn.y₃₀)
    @printf("Turbulent kinematic viscosity = %3.2e m²/s \n", dyn.nu_rat * μ_liq/ρ_liq)
    @printf("Ratio of turbulent and fluid kinematic viscosities = %3.2e \n", dyn.nu_rat)

    @printf("Turbulent scale length = %3.2e m \n", like.ℓ)
    @printf("Turbulence intensity = %3.2e \n", like.I)
    @printf("Specific turbulence kinetic energy = %3.2e m² s⁻² \n", like.k)
    @printf("Eddy dissipation (epsilon) = %3.2e m² s⁻³ \n", like.ϵ)

    # Eddy size, velocity, and time scales.
    @printf("Kolmogorov eddy size = %3.2e m \n", sc.ℓ_Kol)
    @printf("Kolmogorov eddy velocity = %3.2e m/s \n", sc.v_Kol)
    @printf("Kolmogorov eddy time = %3.2e s \n", sc.t_Kol)

    @printf("Taylor eddy size = %3.2e m \n", sc.ℓ_Tay)
    @printf("Taylor eddy velocity = %3.2e m/s \n", sc.v_Tay)
    @printf("Taylor eddy time = %3.2e s \n", sc.t_Tay)

    @printf("Integral eddy size = %3.2e m \n", sc.ℓ_Int)
    @printf("Integral eddy velocity = %3.2e m/s \n", sc.v_Int)
    @printf("Integral eddy time = %3.2e s \n", sc.t_Int)

    # Fluid dynamics/turbulence frequencies.
    @printf("Eddy frequency (omega) = %3.2e s⁻¹ \n", frq.ω) 
    @printf("Kolmogorov eddy frequency = %3.2e s⁻¹ \n", frq.f_Kol)
    @printf("Taylor eddy frequency = %3.2e s⁻¹ \n", frq.f_Tay)
    @printf("Integral eddy frequency = %3.2e s⁻¹ \n", frq.f_Int)
end


test()
print_res()
