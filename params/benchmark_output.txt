ans =

    'Characteristic length = 2.50e-02 m'


ans =

    'Characteristic velocity = 4.67e+00 m/s'


ans =

    'Temperature = 4.00e+02 K'


ans =

    'Pressure = 5.70e+05 Pa'


ans =

    'Density = 1.77e+02 kg/m3'


ans =

    'Dynamic viscosity = 3.88e-05 kg/m-s'


ans =

    'Kinematic viscosity = 2.19e-07 m2/s'


ans =

    'Reynolds number = 5.33e+05'


ans =

    'Max. mean turbulence velocity = 5.84e+00 m/s'


ans =

    'Entrance length for turbulent flow = 9.18e-01 m'


ans =

    'L_e/x_char = 3.67e+01'


ans =

    'Wall friction C_f or f = 3.07e-03'


ans =

    'Wall friction velocity (u*) = 1.83e-01 m/s'


ans =

    'Wall shear = 5.93e+00 kg/m-s2'


ans =

    'y at y+=1 = 1.20e-06 m'


ans =

    'y at y+=7 = 8.38e-06 m'


ans =

    'y at y+=30 = 3.59e-05 m'


ans =

    'Turbulent kinematic viscosity = 2.10e-04 m2/s'


ans =

    'Ratio of turbulent and fluid kinematic viscosities = 9.60e+02'


ans =

    'Turbulence intensity = 2.10e-02'


ans =

    'Specific turbulence kinetic energy = 1.44e-02 m2/s2'


ans =

    'Eddy dissipation (epsilon) = 8.91e-02 m2/s3'


ans =

    'Kolmogorov eddy size = 1.85e-05 m'


ans =

    'Kolmogorov eddy velocity = 1.18e-02 m/s'


ans =

    'Kolmogorov eddy time = 1.57e-03 s'


ans =

    'Taylor eddy size = 5.95e-04 m'


ans =

    'Taylor eddy velocity = 9.81e-02 m/s'


ans =

    'Taylor eddy time = 6.07e-03 s'


ans =

    'Integral eddy size = 1.75e-03 m'


ans =

    'Integral eddy velocity = 1.20e-01 m/s'


ans =

    'Integral eddy time = 1.46e-02 s'


ans =

    'Eddy frequency (omega) = 6.86e+01 1/s'


ans =

    'Kolmogorov eddy frequency = 6.38e+02 1/s'


ans =

    'Taylor eddy frequency = 1.65e+02 1/s'


ans =

    'Integral eddy frequency = 6.86e+01 1/s'