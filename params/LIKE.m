%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                    %
% Program to calculate the "LIKE" algorithm and y+.  The integral, Taylor, and Kolmogorov eddy       %
% velocity, length, and time scales are calculated as well.                                          %
% Turbulence intensity, k, epsilon, omega, and nu_t are also calculated.                             %
%                                                                                                    %
% Units are in SI, unless otherwise noted.                                                           %
%                                                                                                    %
% Assumes fully-developed internal pipe flow.                                                        %
% Can also do external flow over a flat plate, but apply caution to the results.                     %
%                                                                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                                    %
% Programmed by Sal B. Rodriguez on April 21, 2014.  Version 1.0.                                    %
% Revised on December 9, 2015 to correct a few bugs and add more capacity.  Version 1.5.             %
% Modified on 02/15/2016 to improve output readability.  Version 2.0.                                %
%                                                                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  !!!REQUIRED INPUT!!!
%  - Some form of velocity or mass flow rate, e.g., U_inf, u_max, m_dot. 
%  - Pressure P is required for physical properties that are pressure dependent (if using Refprop).
%  - Fluid temperature, T_fluid if using Refprop.
%  - If not using Refprop, then the physical properties must be input manually.
%  - Some form of characteristic distance, e.g., pipe diameter D, L, or x_char. 
%  ---See sample input below for additional input format and requirements (Case 1).
%
%  CHECK ANY LINES WITH "!!!".
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nu = fluid kinematic viscosity
% A = flow area
% flow = 0; %%% Flow is internal
% flow = 1; %%% Flow is external
% u_bar = average turbulent velocity
% u_char = fluid characteristic velocity
% u_bar_max = maximum turbulent velocity
% Re = hydraulic Reynolds number
% I = turbulence intensity
% lambda = surface roughness.
% k = turbulent kinetic energy
% k_liq = thermal conductivity (not k, as in "turbulent kinetic energy"!)
% epsilon = turbulent dissipation
% omega = turbulent frequency
% nu_t = turbulent kinematic viscosity
% C_f = wall skin friction coefficient
% tau_wall = wall shear friction
% u_star = turbulence velocity at the wall
% y_lam7 = total distance from the wall covered by 0 < y_plus < 7
% y_plus_max = maximum y_plus at pipe center
% y_at_log = minimum distance at which log law applies (y_plus=30)
% l_Kol = Kolmogorov eddy size
% v_Kol = Kolmogorov eddy velocity
% t_Kol = Kolmogorov eddy time
% l_Tay = Taylor eddy size
% v_Tay = Taylor eddy velocity
% t_Tay = Taylor eddy time
% l_Int = Integral eddy size
% v_Int = Integral eddy velocity
% t_Int = Integral eddy time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%!!! Start of Educational and Book Examples.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Book Chapter 3 Example: Calibration numbers for CFD-Online comparison.
%%% 12/26/2017.  SI units.  Fluid = air.  External flow over a flat plate.

%flow = 1;  %%% Flow is external.

%T_fluid = 293.15; % K
%P = 101305;  % Fluid pressure, Pa.
%P_refprop = P/1000.0;  % Refprop requires kPa input.

%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'nitrogen', 'oxygen', 'argon', [0.781,0.210,0.009])
%%%rho_liq = 1.205;
%%%mu_liq = 1.82e-5;
%nu = mu_liq/rho_liq;

%u_char = 1.0;
%x_char = 1.0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Book Matlab example for LIKE calculations.  Suitable for Chapters 3 and 5.
%%% sCO2 nozzle, based on ASME paper GT2019-91791, by Finn and Dogan, 2019.
%%% 04/03/2019, 05/15/2019.
%%% Fluid = sCO2.
%%% Note: T_crit=304.1 K, P_crit=7.38 MPa, and rho_crit=469 kg/m3.

%flow = 0;      %%% Flow is internal.
%lambda = 0.0;  %%% lambda = surface roughness, m.

%P = 29.11e6;            %%% Pa; based on ASME paper.
%P_refprop = P/1000.0;   %%% Divide by 1,000 because Refprop requires kPa input.
%T_fluid = 550 + 273.15; % K; % based on ASME paper. 
%%% If uncommented, the next line calls Refprop to compute the sCO2 properties.  
%%% Alternatively, the user can input the properties manually (which are commented immediately below
%%% the Refprop call line).
%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'co2')
%%% The lines below are commented if Refprop is called to calculate the physical properties.
U_sound = 472.9;
mu_liq = 39e-6;  % Based on ASME paper.
rho_liq = 177;   % Based on ASME paper.

%x_char = 0.025;
%u_char = 4.67;

%nu = mu_liq/rho_liq;
%Ma = u_char/U_sound; %%% Mach number calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Another LIKE example for Book, Chapter 3.  11/03/2017.
%%% Gas, water, and LBE cooled reactor example.
%%% Fluid = He, water, or lead-bismuth eutectic (LBE).  (User specifies the fluid below, based on the three choices).
flow = 0;      %%% Flow is internal.
lambda = 0.0;  %%% lambda = surface roughness, m.

P = 5.7e5; 
P_refprop = P/1000.0; %%%101.3 kPa = 101,300 Pa (refprop requires kPa input)
T_fluid = 400;   
T_film = T_fluid;  

%%%%%%% Choice 1, a call for He gas properties %%%%%%%
% [U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'helium')
nu = mu_liq/rho_liq;

%%%%%%% Choice 2, a call for liquid water properties %%%%%%%
%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'water')
%nu = mu_liq/rho_liq;

%%%%%%% Choice 3, a call for the LBE molten metal properties %%%%%%%
%%% Choose value for i_fluid=4 (LBE selection in function "NaPbBiLBE_FUNC").  
%%% Note: if using another molten metal, then Na=1, Pb=2, and Bi=3.
%%% Otherwise, insert the necessary molten metal properties manually.
%i_fluid = 4;      %%% Choice of fluid. Default = 0, which will induce abort.
%i_matprop = 1;    %%% Choice of method to calculate the material properties. 
                   %%% 1 = Function call for Na, Pb, Bi, or LBE properties.
%if i_matprop == 1
    %%% Call function "NaPbBiLBE_FUNC" to obtain the fluid material properties.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% How function "NaPbBiLBE_FUNC" works:
    %%% values=values(i_fluid, T)= array with fluid temperature, T_fluid.
    %%% [rho, beta, Cp_liq, mu, k, Pr, nu, alpha] = output from function NaProperties07142015.
    %%% i_fluid=values(1);   %%% Choice of fluid. Na=1, Pb=2, Bi=3, and LBE=4
    %%% T = values(2);       %%% T=T_fluid, the fluid temperature.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %values = [i_fluid, T_fluid]
    %[rho_liq, beta, Cp_liq, mu_liq, k_liq, Pr, nu, alpha_liq] = NaPbBiLBE_FUNC(values)
    %nu = mu_liq/rho_liq;
%end

u_fluid = 5.5;
u_char = u_fluid;
%Ma = u_char/U_sound; %%% Mach number calculation
D_pipe = 0.1;
x_char = D_pipe;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Book example for nitrogen gas flow.  Example 3.3.  06/15/2019.
%%% Fluid = N2.
%flow = 0; %%% Flow is internal.
%lambda = 0.0;  %%% lambda = surface roughness, m.

%P = 5.0e6;        % Based on ASME paper.
%P_refprop = P/1000.0;  %%%101 kPa = 101,000 Pa (refprop requires kPa input)
%T = 600; %
%%% Call for nitrogen.  
%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T,'P',P_refprop,'nitrogen')
%nu = mu_liq/rho_liq;

%x_char = 0.025;
%u_char = 2.0;

%Ma = u_char/U_sound; %%% Mach number calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Example 3.6A.  Turbulent pipe flow.  Fluid = water.  10/16/2020.
%flow = 0;    %%% Flow is internal.
%lambda = 0.0 %%% lambda = surface roughness, m.

%P = 5.7e5;   %%% Pa
%P_refprop = P/1000.0;   %%% Divide by 1,000 because Refprop requires kPa input.
%T_fluid = 350; % K
%%% Call for water, where T and P are specified:
%[rho_liq k_liq mu_liq Cp_liq Pr beta U_sound] = refpropm('DLVC^BA','T',T_fluid,'P',P_refprop,'water')  

%x_char = 0.1;
%u_char = 5.5;

%nu = mu_liq/rho_liq;
%Ma = u_char/U_sound; %%% Mach number calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Book example for high P and T water.  Example 4.3.  05/14/2018.
%flow = 0; %%% Flow is internal.
%lambda = 2e-7; %%%0.0;  %%% lambda = surface roughness, m.

%P = 7.5e6;  % Fluid pressure, Pa. Assume one ATM.
%P_refprop = P/1000.0;  %%%101.3 kPa = 101,300 Pa (refprop requires kPa input)
%T_fluid = 500;  %%% % K.
%T_film = T_fluid;
%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'water')
%nu = mu_liq/rho_liq;

%D = 0.0254; 
%x_char = D;
%Re = 3000;
%u_char = nu*Re/x_char  %%%%u_char = 12.5;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Book example for high P and T water.  Example 4.4.  05/14/2018.
%flow = 0; %%% Flow is internal.
%lambda = 2e-7; %%% lambda = surface roughness, m.

%P = 7.5e6;  % Fluid pressure, Pa. Assume one ATM.
%P_refprop = P/1000.0;  %%%101.3 kPa = 101,300 Pa (refprop requires kPa input)
%T_fluid = 500;  %%% % K.
%T_film = T_fluid;
%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'water')
%nu = mu_liq/rho_liq;

%D = 0.0254; 
%x_char = D;

%Re = 3000;
%u_char = nu*Re/x_char;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Book example for high P and T water.  Example 4.5.  05/14/2018.
%flow = 0; %%% Flow is internal.
%lambda = 2e-7; %%%0.0;  %%% lambda = surface roughness, m.

%P = 7.5e6;  % Fluid pressure, Pa. Assume one ATM.
%P_refprop = P/1000.0;  %%%101.3 kPa = 101,300 Pa (refprop requires kPa input)
%T_fluid = 500;  %%% % K.
%T_film = T_fluid;
%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'water')
%nu = mu_liq/rho_liq;

%D = 0.0254; 
%x_char = D;

%Re_T_SKE = Re_T_SKE = k^2/(epsilon*nu) = 2.2;
%u_char = 0.00012; %%% Assume u_char; solve by iterating.  Iterative solution yields u_char = 0.00012.  See Book, page 186.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Book Ch. 5 type of example for DNS, LES, and RANS problems. 08/13/2018.
%flow = 0; %%% Flow is internal.
%lambda = 0.0;  %%% lambda = surface roughness, m.

%P = 1.0135e5;  % Fluid pressure, Pa. Assume one ATM.
%P_refprop = P/1000.0;  %%%101.3 kPa = 101,300 Pa (refprop requires kPa input)
%T_fluid = 500;  %%% % K.
%T_film = T_fluid;
%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'water')

%%%rho_liq = 91.0; %
%%%mu_liq = 3.6e-5;

%nu = mu_liq/rho_liq;

%D = 0.0032; 
%x_char = D;

%u_char = 0.7; %%% Assume u_char.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Example 5.1.  Turbulent pipe flow.  Fluid = helium.  05/018/2019. 
%flow = 0;    %%% Flow is internal.
%lambda = 0.0 % 1.0e-4; %%% lambda = surface roughness, m.

%P = 6e6;            %%% Pa
%P_refprop = P/1000.0;   %%% Divide by 1,000 because Refprop requires kPa input.
%T_fluid = 400; % K
%%%% If uncommented, the next line calls Refprop to compute the sCO2 properties.  
%%%% Alternatively, the user can input the properties manually (which are commented immediately below
%%%% the Refprop call line).
%[U_sound rho_liq mu_liq] = refpropm('ADV','T',T_fluid,'P',P_refprop,'helium')

%%%% The lines below are commented if Refprop is called to calculate the physical properties.
%%%%U_sound = 472.9;
%%%%mu_liq = 38.8e-6;  
%%%%rho_liq = 177.2;   

%x_char = 0.125;
%u_char = 1.4;

%nu = mu_liq/rho_liq;
%Ma = u_char/U_sound; %%% Mach number calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Book Ch. 5 for RANS, Part 2:  Calculate for NENE 2020 paper. 09/11/2020.
% Calculate for both water and air at 300 K and 1 atm.
%flow = 0; %%% Flow is internal.
%lambda = 0.0;  %%% lambda = surface roughness, m.

%P = 1.0135e5;  % Fluid pressure, Pa. Assume one ATM.
%P_refprop = P/1000.0;  %%%101.3 kPa = 101,300 Pa (refprop requires kPa input)
%T_fluid = 300;  %%% K.

% Use for air properties.
%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'nitrogen', 'oxygen', 'argon', [0.781,0.210,0.009])

% Use for water properties.
%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_fluid,'P',P_refprop,'water')

%nu = mu_liq/rho_liq;

%D = 0.0032; 
%x_char = D;

%u_char = 2.1425  %%%%21.4250  %%%%267.8125  %%%700.0  %%%70.0  %%%7.0   %%%0.7; %%% Assume u_char.
%Ma = u_char/U_sound; %%% Mach number calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Example 6D.  Turbulent flat plate flow.  Fluid = water.  10/16/2020.
%flow = 0;    %%% Flow is internal.
%lambda = 0.0 %%% lambda = surface roughness, m.

%P = 1.0e6; % Pa
%P_refprop = P/1000.0;   %%% Divide by 1,000 because Refprop requires kPa input.
%T_fluid = 395; %K
%%% Call for water, where T and P are specified:
%[rho_liq k_liq mu_liq Cp_liq Pr beta U_sound] = refpropm('DLVC^BA','T',T_fluid,'P',P_refprop,'water')  

%%%T = 394.2 K.
%%%rho_liq = 942;

%x_char = 0.1;
%u_char = 700;

%nu = mu_liq/rho_liq;
%Ma = u_char/U_sound; %%% Mach number calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example 10.
%flow = 0; %%% Flow is internal.
%lambda = 0.0;  %%% lambda = surface roughness, m.

%rho_liq = 15.0;
%mu_liq = 3.89e-5;
%nu = mu_liq/rho_liq;

%D = 0.1;
%x_char = D;
%m_dot = 0.05;
%A = (pi/4)*D^2;
%u_bar = m_dot/(rho_liq*A);
%u_char = u_bar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Couette flow problem for Fall 2015 class.
%flow = 0; %%% Flow is internal.
%lambda = 2e-7; %%%0.0;  %%% lambda = surface roughness, m.

%mu_liq = 52.6e-6; % kg/m-s
%rho_liq = 2.79; % kg/m^3
%nu = mu_liq/rho_liq;

%u_wall = 20.;
%u_char = u_wall;
%D = 0.1; %%% 0.1 for 1 moving wall separated by "H"; 0.05 for 2 moving walls separated by h.
%H = D;  %%%
%x_char = H;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DNS 3D rectangle w/ hypothetical low Pr fluid.  09/13/2016.
%%% SI units.
%flow = 0; %%% Flow is internal.
%lambda = 1e-6; %%% lambda = surface roughness, m.

%mu_liq = 5e-4 % kg/m-s
%k_liq = 75.0
%Cp_liq = 3750.
%rho_liq = 5000 % kg/m^3
%nu = mu_liq/rho_liq;
%Pr = Cp_liq*mu_liq/k_liq;

%u_fluid = 0.5;
%u_char = u_fluid
%S = 1.0e-3;
%x_char = S
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Sea creatures example, under low tide.  12/19/2016.
%%% Fluid = h2o.
%%% This flow is LAMINAR, hence the LIKE algorithm is NOT applicable!!  Increase Re to apply LIKE.
%flow = 1; %%% Flow is external.

%P = 101300.0;
%P_refprop = P/1000.0;  %%%101.3 kPa = 101,300 Pa (refprop requires kPa input)
%T_fluid = 280;   % Fluid temperature, K.  
%T_film = T_fluid;

%[U_sound rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_film,'P',P_refprop,'water')
%nu = mu_liq/rho_liq;

%u_fluid = 0.1;  %%% Based on 1.9 million hexahedral elements using Fuego simulation, 03/11/2016.
%u_char = u_fluid;
%Ma = u_char/U_sound;  %%%Of course,it would be a SHOCK (literally and figuratively) if this flow were compressible!
%D = 0.001;
%x_char = D;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Mako shark swimming at cruise (25 mph) and hunting (42 mph) speeds.  10/04/2017.
%%% Fluid = water.
%flow = 1; %%% Flow is external.

%P = 101300.0;  % Fluid pressure, Pa. Assume one ATM.
%P_refprop = P/1000.0;  %%%101.3 kPa = 101,300 Pa (refprop requires kPa input)
%T_fluid = 280;   % Fluid temperature, cool water.
%T_film = T_fluid;

%%% Call for water; specify T and P (ignore salinity effect on density and viscosity).
%[U_sound, rho_liq k_liq mu_liq Cp_liq Pr beta] = refpropm('ADLVC^B','T',T_film,'P',P_refprop,'water')
%nu = mu_liq/rho_liq;

%%%u_fluid = 25*(5280/1)*(1/3.2808)*(1/3600);  %%% Cruise speed.  Shark speed is sustained at 25 mph, converted to m/s.
%u_fluid = 42*(5280/1)*(1/3.2808)*(1/3600);  %%% Hunting speed.  Shark speed max burst speed is 42 mph, converted to m/s.
%u_char = u_fluid;
%Ma = u_char/U_sound; %%% Mach number calculation. Of course,it would be a SHOCK (literally and figuratively) if this flow were compressible!

%%% Estimate shark's effective diameter.
%mass_shark = 600; %%% kg
%rho_shark = 1000; %%% Assume water density, kg/m3.
%L_shark = 3.2; %%% m
%%% Use the above three parameters to estimate the shark diamter.
%%% That is, mass = volume*density = area*length*density, where Area=pi*D_shark*D_shark/4.
%D_shark = sqrt(4*mass_shark/(pi*rho_shark*L_shark));
%x_char = D_shark;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%!!! End of Educational and Book Examples.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Begin calculations.                                                                            %%%                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Fixed constants.
y_plus1 = 1.0;   %%% Used to obtain y at y+ = 1 for meshing purposes.
y_plus7 = 7.0;   %%% Laminar viscous sublayer; 7 is ~ max., but can be as little as 5-ish.
y_plus30 = 30.0; %%% Value for beginning of log domain.
C_mu = 0.09;
beta_star = 9./100.;  %%% Wilcox k-omega model.

%%% Begin turbulence calculations.

%%% A = pi*D*D/4;
%%% m_dot = rho_liq*u_fluid*A;
%%% u_bar = m_dot/(rho_liq*A);

u_bar = u_char;             %%% u_fluid;
u_bar_max = (5./4.)*u_bar;  %%% This line is correct iff u_bar is an average velocity.  BSL expression.
u_char_max = u_bar_max;
Re = x_char*u_char/nu;

l = 0.07*x_char;   %%% Do not use l=C_mu*k^(3/2)/epsilon here.
%%% Make this expression obsolete as of 10/16/2020: I = 0.16*Re^(-1/8).  Instead, use Sanborn's 1955 expression:
%%%I = 0.16*Re^(-1./8.);
I = 0.144*Re^(-0.146);
k = (3./2.)*(u_char*I)^2;
epsilon = C_mu*(k^(3./2.))/l;  %%% Uses "l" based on l=C*x_char.  This is the Prandtl-Kolmogorov relationship.

omega = epsilon/(beta_star*k);
nu_t = (C_mu*k^2)/epsilon;
nu_rat = nu_t/nu;

% Kolmogorov Eddies.
l_Kol = ( (nu)^3/epsilon )^(1/4);
t_Kol = (nu/epsilon)^(1/2);
v_Kol =l_Kol/t_Kol;

% Taylor Eddies.
l_Tay = ( (10*k*nu)/epsilon )^(1/2);
t_Tay = (15*nu/epsilon)^(1/2);
v_Tay = l_Tay/t_Tay;

% Integral Eddies
l_Int = l; %%% Fast and easy estimate formula is used vs. Equation 3.1B = C_mu*(k^3/2)/epsilon.
t_Int = C_mu*k/epsilon;
v_Int = l_Int/t_Int;

if flow == 1 %%% Flow is external.
    if Re <= 1.0e9
         C_f_ext = (2*log10(Re) - 0.65)^(-2.3);  %%%Schlichting skin friction equation for flat plate.  Good for Re < 1x10^9.
         C_f = C_f_ext;
    else
        disp('Error: Re outside of wall friction correlation range') 
    end
elseif flow == 0 %%% Flow is internal.
        C_f_int = 0.0055*( 1+(2.0e4*(lambda/x_char)+ 1.0e6/Re)^(1/3)  );
        C_f = C_f_int/4;  %%% Convert the Darcy value to Fanning friction factor.
    else
        disp('Error: Unknown flow type') 
end

tau_wall = C_f*rho_liq*u_char*u_char/2;
u_star = sqrt(tau_wall/rho_liq);   %%% friction velocity;  aka shear velocity (hence a "tau" instead of a "star").

y_at_yplus1 = y_plus1*nu/u_star;
y_at_yplus7 = y_plus7*nu/u_star;
y_at_yplus30 = y_plus30*nu/u_star;

%%% Estimation of entrance length to reach fully developed turbulent flow.
n1 = 0.25;
C1 = 1.359;
L_e = x_char*C1*Re^n1;
L_over_x_char = L_e/x_char;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Print all relevant turbulence quantities...

%%% Characteristic length and velocity.
sprintf('Characteristic length = %3.2e m', x_char)
sprintf('Characteristic velocity = %3.2e m/s', u_char)

%%% Fluid properties.
sprintf('Temperature = %3.2e K', T_fluid)
sprintf('Pressure = %3.2e Pa', P)
sprintf('Density = %3.2e kg/m3', rho_liq)
sprintf('Dynamic viscosity = %3.2e kg/m-s', mu_liq)
sprintf('Kinematic viscosity = %3.2e m2/s', nu)
%sprintf('Sound speed = %3.2e m/s', U_sound)

%%% Dimensionless numbers.
sprintf('Reynolds number = %3.2e', Re)
%sprintf('Mach number = %3.2e ', Ma)

%%% Key Fluid dynamics and turbulence parameters.
sprintf('Max. mean turbulence velocity = %3.2e m/s', u_char_max)
sprintf('Entrance length for turbulent flow = %3.2e m', L_e)
sprintf('L_e/x_char = %3.2e', L_over_x_char)
sprintf('Wall friction C_f or f = %3.2e', C_f)
sprintf('Wall friction velocity (u*) = %3.2e m/s', u_star)
sprintf('Wall shear = %3.2e kg/m-s2', tau_wall)
sprintf('y at y+=1 = %3.2e m', y_at_yplus1)
sprintf('y at y+=7 = %3.2e m', y_at_yplus7)
sprintf('y at y+=30 = %3.2e m', y_at_yplus30)
sprintf('Turbulent kinematic viscosity = %3.2e m2/s', nu_t)
sprintf('Ratio of turbulent and fluid kinematic viscosities = %3.2e', nu_rat)
sprintf('Turbulence intensity = %3.2e', I)
sprintf('Specific turbulence kinetic energy = %3.2e m2/s2', k)
sprintf('Eddy dissipation (epsilon) = %3.2e m2/s3', epsilon)

%%% Eddy size, velocity, and time scales.
sprintf('Kolmogorov eddy size = %3.2e m', l_Kol)
sprintf('Kolmogorov eddy velocity = %3.2e m/s', v_Kol)
sprintf('Kolmogorov eddy time = %3.2e s', t_Kol)

sprintf('Taylor eddy size = %3.2e m', l_Tay)
sprintf('Taylor eddy velocity = %3.2e m/s', v_Tay)
sprintf('Taylor eddy time = %3.2e s', t_Tay)

sprintf('Integral eddy size = %3.2e m', l_Int)
sprintf('Integral eddy velocity = %3.2e m/s', v_Int)
sprintf('Integral eddy time = %3.2e s', t_Int)

%%% Fluid dynamics/turbulence frequencies.
sprintf('Eddy frequency (omega) = %3.2e 1/s', omega)
sprintf('Kolmogorov eddy frequency = %3.2e 1/s', 1/t_Kol)
sprintf('Taylor eddy frequency = %3.2e 1/s', 1/t_Tay)
sprintf('Integral eddy frequency = %3.2e 1/s', 1/t_Int)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%